/*
* Copyright (c) 2016 Carnegie Mellon University, Author <ar.sankalp@gmail.com>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <trajectory_visualization/Trajectory.h>
#include <trajectory_visualization/PathXYZVPsi.h>
#include <trajectory_visualization/PathXYZVViewPoint.h>

ros::Publisher marker_pub;
visualization_msgs::Marker line_strip;
std::string ns;
double r,g,b,scale;

void TrajectoryCallback ( const trajectory_visualization::Trajectory::ConstPtr& traj_msg )
{
   std::string temp_ns = ns;
   line_strip.header.frame_id =  traj_msg->header.frame_id;
   line_strip.header.stamp = traj_msg->header.stamp;
   line_strip.ns = temp_ns.append("_traj");
   line_strip.action = visualization_msgs::Marker::ADD;
   line_strip.type = visualization_msgs::Marker::LINE_STRIP;
   line_strip.pose.orientation.w = 1.0;

   line_strip.id = 1;

   line_strip.scale.x = scale;
   line_strip.color.r = r;
   line_strip.color.g = g;
   line_strip.color.b = b;
   line_strip.color.a = 1.0;

   geometry_msgs::Point p;
   for(size_t i=0;i<traj_msg->trajectory.size();i++){
   p.x = traj_msg->trajectory[i].position.x;
   p.y = traj_msg->trajectory[i].position.y;
   p.z = traj_msg->trajectory[i].position.z;
   line_strip.points.push_back ( p );
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();

   temp_ns = ns;
   line_strip.id = 1;
   line_strip.type = visualization_msgs::Marker::LINE_LIST;
   line_strip.ns = temp_ns.append("_traj_vel");
   geometry_msgs::Vector3 v;
   for(size_t i=0;i<traj_msg->trajectory.size();i++){
     p = traj_msg->trajectory[i].position;
     line_strip.points.push_back ( p );
     v = traj_msg->trajectory[i].velocity;
     p.x = p.x+v.x; p.y = p.y+v.y; p.z = p.z+v.z;
     line_strip.points.push_back ( p );
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();

   temp_ns = ns;
   line_strip.id = 1;
   line_strip.type = visualization_msgs::Marker::LINE_LIST;
   line_strip.ns = temp_ns.append("_traj_heading");
   //geometry_msgs::Vector3 v;
   for(size_t i=0;i<traj_msg->trajectory.size();i++){
       p = traj_msg->trajectory[i].position;
       line_strip.points.push_back ( p );
       //v = path_msg->trajectory[i].velocity;
       p.x = p.x+5*cos(traj_msg->trajectory[i].heading);
       p.y = p.y+5*sin(traj_msg->trajectory[i].heading);
       line_strip.points.push_back ( p );
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();
}

void PathCallback ( const trajectory_visualization::PathXYZVPsi::ConstPtr& path_msg )
{
   std::string temp_ns = ns;
   line_strip.header.frame_id =  path_msg->header.frame_id;
   line_strip.header.stamp = path_msg->header.stamp;
   line_strip.ns = temp_ns.append("_path");
   line_strip.action = visualization_msgs::Marker::ADD;
   line_strip.type = visualization_msgs::Marker::LINE_STRIP;
   line_strip.pose.orientation.w = 1.0;

   line_strip.id = 1;

   line_strip.scale.x = scale;
   line_strip.color.r = r;
   line_strip.color.g = g;
   line_strip.color.b = b;
   line_strip.color.a = 1.0;

   geometry_msgs::Point p;
   for(size_t i=0;i<path_msg->waypoints.size();i++){
   p = path_msg->waypoints[i].position;
   line_strip.points.push_back ( p );
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();

   temp_ns = ns;
   line_strip.id = 1;
   line_strip.type = visualization_msgs::Marker::LINE_LIST;
   line_strip.ns = temp_ns.append("_path_heading");
   double v;
   for(size_t i=0;i<path_msg->waypoints.size();i++){
       p = path_msg->waypoints[i].position;
       line_strip.points.push_back ( p );
       v = path_msg->waypoints[i].vel;
       p.x = p.x+v*cos(path_msg->waypoints[i].heading);
       p.y = p.y+v*sin(path_msg->waypoints[i].heading);
       line_strip.points.push_back ( p );
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();

   //line_strip
}


void ViewPointPathCallback ( const trajectory_visualization::PathXYZVViewPoint::ConstPtr& path_msg )
{
   std::string temp_ns = ns;
   line_strip.header.frame_id =  path_msg->header.frame_id;
   line_strip.header.stamp = path_msg->header.stamp;
   line_strip.ns = temp_ns.append("_viewpoint_path");
   line_strip.action = visualization_msgs::Marker::ADD;
   line_strip.type = visualization_msgs::Marker::LINE_STRIP;
   line_strip.pose.orientation.w = 1.0;

   line_strip.id = 1;

   line_strip.scale.x = scale;
   line_strip.color.r = r;
   line_strip.color.g = g;
   line_strip.color.b = b;
   line_strip.color.a = 1.0;

   geometry_msgs::Point p;
   for(size_t i=0;i<path_msg->waypoints.size();i++){
       p = path_msg->waypoints[i].position;
       line_strip.points.push_back ( p );
       if(i>0){
           geometry_msgs::Point pp = line_strip.points[i-1];
           double d = std::sqrt(std::pow((p.x - pp.x),2) + std::pow((p.y - pp.y),2)
                                + std::pow((p.z - pp.z),2));
           ROS_ERROR_STREAM("Distance between "<<i<<"::"<<d);
       }
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();

   temp_ns = ns;
   line_strip.id = 1;
   line_strip.type = visualization_msgs::Marker::CUBE_LIST;
   line_strip.scale.x = line_strip.scale.y = line_strip.scale.z = 5*scale;
   line_strip.color.r = r;
   line_strip.color.g = g;
   line_strip.color.b = b;
   line_strip.ns = temp_ns.append("_viewpoint_path_type_safe");
   //geometry_msgs::Vector3 v;
   for(size_t i=0;i<path_msg->waypoints.size();i++){
       if(path_msg->waypoints[i].safety ==
               trajectory_visualization::XYZVViewPoint::SAFETY_MODE_FORWARD){
           line_strip.points.push_back (path_msg->waypoints[i].position);
       }
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();

   temp_ns = ns;
   line_strip.id = 1;
   line_strip.type = visualization_msgs::Marker::SPHERE_LIST;
   line_strip.scale.x = line_strip.scale.y = line_strip.scale.z = 5*scale;
   line_strip.ns = temp_ns.append("_viewpoint_path_type_fov");
   //geometry_msgs::Vector3 v;
   for(size_t i=0;i<path_msg->waypoints.size();i++){
       if(path_msg->waypoints[i].safety ==
               trajectory_visualization::XYZVViewPoint::SAFETY_MODE_FOV){
           line_strip.points.push_back (path_msg->waypoints[i].position);
       }
   }
   marker_pub.publish ( line_strip );
   line_strip.points.clear();
   //line_strip


}


int main ( int argc, char **argv )
{
   ros::init ( argc, argv, "trajectory_visualizer" );
   ros::NodeHandle np ( "~" );
   std::string trajectory_topic;
   std::string marker_topic;
   std::string path_topic;
   std::string viewpoint_path_topic;

   bool got_params = true;
   got_params = got_params && np.getParam("trajectory_topic",trajectory_topic);
   got_params = got_params && np.getParam("path_topic",path_topic);
   got_params = got_params && np.getParam("viewpoint_path_topic",viewpoint_path_topic);
   got_params = got_params && np.getParam("marker_topic",marker_topic);
   got_params = got_params && np.getParam( "r", r);
   got_params = got_params && np.getParam( "g", g);
   got_params = got_params && np.getParam( "b", b);
   got_params = got_params && np.getParam( "scale", scale);
   got_params = got_params && np.getParam( "namespace", ns);
   if(!got_params){
       ROS_ERROR_STREAM("Trajectory visualization did not get all the parameters.");
       exit(-1);
   }
   ros::Subscriber sub = np.subscribe ( trajectory_topic, 100, TrajectoryCallback );
   ros::Subscriber path_sub = np.subscribe ( path_topic, 100, PathCallback);
   ros::Subscriber viewpoint_path_sub = np.subscribe ( viewpoint_path_topic, 100, ViewPointPathCallback);

   marker_pub = np.advertise<visualization_msgs::Marker> ( marker_topic, 10 );

   ros::spin();

   return 0;
}
